package edu.westga.cs1302.project2.tests.daily;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.project2.model.Daily;

/**
 * Ensures correct functionality of the occursOn method.
 * @author Angel Hernandez
 * @version 10/17/2021
 */
public class TestOccursOn {

	@Test
	public void testOccursOnTheDayOf() {
		LocalDate date = LocalDate.now();
		Daily aAppointment = new Daily(date, "Check up");
		
		assertEquals(true, aAppointment.occursOn(date));

	}

	@Test
	public void testOccursOnFutureDate() {
		LocalDate date = LocalDate.of(2021, 11, 11);
		Daily aAppointment = new Daily(date, "Check up");
		
		assertEquals(true, aAppointment.occursOn(date));

	}
	
	@Test
	public void testOccursOnPastDate() {
		assertThrows(IllegalArgumentException.class, 
						()->{
							LocalDate date = LocalDate.of(2020, 11, 11);
							Daily aAppointment = new Daily(date, "Check up");
							aAppointment.occursOn(date);
						}
					);
	}
}
