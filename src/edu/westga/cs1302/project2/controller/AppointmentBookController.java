package edu.westga.cs1302.project2.controller;

import edu.westga.cs1302.project2.model.AppointmentBook;

/**
 * AppointmentBookController generates random appointments and gets appointments grouped by
 * types.
 * 
 * @author	CS1302
 * @version	Fall 2021
 */
public class AppointmentBookController {

	private static String[] descriptions = { "dental appointment", "car maintenance", "dance lessons",
		"drawing lessongs", "birthday party", "grocery shopping" };

	private AppointmentBook appointmentBook;

	/**
	 * Instantiates a new appointment book controller.
	 *
	 * @precondition: none
	 * @postcondition: getAppointmentBook().size()==0
	 */
	public AppointmentBookController() {
		this.appointmentBook = new AppointmentBook();
	}

	/**
	 * Part 3 TODO: Generate random appointments and add them into the appointmentBook.
	 * 
	 * Randomly generate a number between 20 and 30. Create that number of Single,
	 * Monthly, Daily and Yearly appointments
	 * 
	 * For each appointment, the date should be a random number within 0-364 days after
	 * today, the description should be any random description in the descriptions
	 * array, the nmberOfYears should be a random number b/w 1 to the length of the
	 * descriptions array, the priority should be a random number b/w 1 - 10.
	 * 
	 * @precondition: none
	 * @postcondition: appointmentBook has 20-30 randomly generated appointments of each type
	 * 
	 */
	public void generateRandomAppointments() {

	}

	/**
	 * Gets the appointment book.
	 * 
	 * @precondition: none
	 * @postcondition: none
	 * 
	 * @return the appointment book
	 */
	public AppointmentBook getAppointmentBook() {
		return this.appointmentBook;
	}

}
