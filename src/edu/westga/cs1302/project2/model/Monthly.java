package edu.westga.cs1302.project2.model;

import java.time.LocalDate;

/**
 * The class Monthly represents appointments that occur on the same day of every month since the initial appoinmentDate.
 * 
 * @author Angel Hernandez
 * @version 10/17/2021
 */
public class Monthly extends Appointment {

	/**
	 * Instantiates a new Monthly appointment.
	 * 
	 * @precondition date != null && date cannot be earlier than today.
	 * 				 description != null && !description.isEmpty()
	 * @postcondition getDate() == date && getDescription() == description
	 * 
	 * @param appointmentDate - the date.
	 * @param description - the description.
	 */
	public Monthly(LocalDate appointmentDate, String description) {
		super(appointmentDate, description);
	}

	/**
	 * Instantiates a new Monthly appointment.
	 * 
	 * @precondition description != null && !description.isEmpty()
	 * @postcondition getDescription() == description
	 * 
	 * @param description - the description
	 */
	public Monthly(String description) {
		this(LocalDate.now(), description);
	}
	
	@Override
	public int hashCode() {
		return super.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Monthly []";
	}

	@Override
	public boolean occursOn(LocalDate date) {
		// TODO Auto-generated method stub
		return false;
	}

}
