package edu.westga.cs1302.project2.model;

import java.time.LocalDate;
import java.util.Objects;

import edu.westga.cs1302.project2.resources.UI;

/**
 * The Class Appointment.
 * 
 * @author Angel Hernandez
 * @version 10/15/2021
 */
public abstract class Appointment {
	
	private LocalDate appointmentDate;
	private String description;

	/**
	 * Instantiates a new Appointment.
	 * 
	 * @precondition date != null && date cannot be earlier than today.
	 * 				 description != null && !description.isEmpty()
	 * @postcondition getDate() == date && getDescription() == description
	 * 
	 * @param date - the date.
	 * @param description - the description
	 */
	protected Appointment(LocalDate date, String description) {
		if (date == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NULL_DATE);
		}
		if (date.isBefore(LocalDate.now())) {
			throw new IllegalArgumentException(UI.ExceptionMessages.EARLIER_THAN_TODAY);
		}
		if (description == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NULL_DESCRIPTION);
		}
		if (description.isEmpty()) {
			throw new IllegalArgumentException(UI.ExceptionMessages.EMPTY_DESCRIPTION);
		}
		this.appointmentDate = date;
		this.description = description;
	}

	/**
	 * Instantiates a new Appointment.
	 * 
	 * @precondition description != null && !description.isEmpty()
	 * @postcondition getDescription() == description
	 * 
	 * @param description - the description
	 */
	protected Appointment(String description) {
		this(LocalDate.now(), description);
		if (description == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NULL_DESCRIPTION);
		}
		if (description.isEmpty()) {
			throw new IllegalArgumentException(UI.ExceptionMessages.EMPTY_DESCRIPTION);
		}
		this.description = description;
	}
	
	
	/**
	 * Compares Appointment objects by their contents instead of their references.
	 * 
	 * @precondition otherObject != null
	 * @postcondition none
	 * @param otherObject - object being compared
	 * @return true, if this object's appointmentDate and description are exactly
	 *		   the same (in terms of value/content equality) as the otherObject's
	 *		   appointmentDate and description, respectively.
	 */
	@Override
	public boolean equals(Object otherObject) {
		if (otherObject == null)
			return false;
		if (this == otherObject)
			return true;
		if (this.getClass() != otherObject.getClass())
			return false;
		Appointment other = (Appointment) otherObject;
		return Objects.equals(this.appointmentDate, other.appointmentDate) && Objects.equals(this.description, other.description);
	}
	
	/**
	 * Gets the date.
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return the date.
	 */
	public LocalDate getDate() {
		return this.appointmentDate;
	}

	/**
	 * Gets the day of the month.
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return the primitive integer value for the day-of-month.
	 */
	public int getDayOfMonth() {
		return this.appointmentDate.getDayOfMonth();
	}
	
	
	/**
	 * Gets the month number.
	 *  
	 * @precondition none
	 * @postcondition none
	 * @return the month as an integer from 1 to 12.
	 */
	public int getMonthNumber() {
		return this.appointmentDate.getMonthValue();
	}
	
	/**
	 * Gets the year.
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return the primitive integer value for the year.
	 */
	public int getYear() {
		return this.appointmentDate.getYear();
	}
	
	/**
	 * Gets the description.
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return the description.
	 */
	public String getDescription() {
		return this.description;
	}
	
	/**
	 * Returns a hash code of an object.
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return a hash code value for the object
	 */
	@Override
	public int hashCode() {
		return Objects.hash(this.appointmentDate, this.description);
	}
	
	@Override
	/**
	 * Returns the string representation for this appointment.
	 * 
	 * @return a string in the format of "yyyy/mm/dd, classname, description".
	 */
	public String toString() {
		return  this.appointmentDate + ", " + getClass().getName() + ", " + this.description;
	}
	
	/**
	 * Checks if the specified date is the initial appointmentDate, 
	 * or it is any date that's later than the initial appointmentDate.
	 * @param date - the date
	 * @return true if it occurs on specified or later date, false otherwise.
	 */
	public abstract boolean occursOn(LocalDate date);	
}
