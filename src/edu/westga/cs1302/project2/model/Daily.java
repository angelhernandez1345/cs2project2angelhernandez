package edu.westga.cs1302.project2.model;

import java.time.LocalDate;

/**
 * The class Daily represents appointments that occur every day since the initial appointmentDate.
 * 
 * @author Angel Hernandez
 * @version 10/17/2021
 */
public class Daily extends Appointment {

	/**
	 * Instantiates a new Daily appointment.
	 * 
	 * @precondition date != null && date cannot be earlier than today.
	 * 				 description != null && !description.isEmpty()
	 * @postcondition getDate() == date && getDescription() == description
	 * 
	 * @param appointmentDate - the date.
	 * @param description - the description.
	 */
	public Daily(LocalDate appointmentDate, String description) {
		super(appointmentDate, description);
	}

	/**
	 * Instantiates a new Daily appointment.
	 * 
	 * @precondition description != null && !description.isEmpty()
	 * @postcondition getDescription() == description
	 * 
	 * @param description - the description
	 */
	public Daily(String description) {
		this(LocalDate.now(), description);
	}
	
	
	/**
	 * Checks if the specified date is the initial appointmentDate, 
	 * or it is any date that's later than the initial appointmentDate.
	 * 
	 * @param date - the date
	 * @return true if it occurs on specified or later date, false otherwise.
	 */
	@Override
	public boolean occursOn(LocalDate date) {
		if (LocalDate.now().equals(date) || date.isAfter(LocalDate.now())) {
			return true;
		}
		return false;
	}
	
	/**
	 * Returns a hash code of Daily object.
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return a hash code value for the Daily object
	 */
	@Override
	public int hashCode() {
		return super.hashCode() + getClass().toString().hashCode();
	}
}
