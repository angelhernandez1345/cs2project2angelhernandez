//package edu.westga.cs1302.project2.model;
//
//import java.time.LocalDate;
//import java.util.Objects;
//
//public class Yearly extends Appointment {
//	private int numberOfYears;
//	
//	public Yearly(LocalDate appointmentDate, String description) {
//		super(appointmentDate, description);
//		
//	}
//
//	public Yearly(String description) {
//		super(description);
//	}
//	
//	public int getNumberOfYears() {
//		return this.numberOfYears;
//	}
//	
//	@Override
//	public int hashCode() {
//		final int prime = 31;
//		int result = super.hashCode();
//		result = prime * result + Objects.hash(numberOfYears);
//		return result;
//	}
//
//	@Override
//	public boolean equals(Object obj) {
//		if (this == obj)
//			return true;
//		if (!super.equals(obj))
//			return false;
//		if (getClass() != obj.getClass())
//			return false;
//		Yearly other = (Yearly) obj;
//		return numberOfYears == other.numberOfYears;
//	}
//
//	@Override
//	public String toString() {
//		return "Yearly [numberOfYears=" + numberOfYears + "]";
//	}
//
//	@Override
//	public boolean occursOn(LocalDate date) {
//		// TODO Auto-generated method stub
//		return false;
//	}
//
//}
