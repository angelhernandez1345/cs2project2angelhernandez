package edu.westga.cs1302.project2.model;

import java.time.LocalDate;
import java.util.Objects;

public class Single extends Appointment {
	private int priority;
	
	public Single(LocalDate appointmentDate, String description, int priority) {
		super(appointmentDate, description);
	}
	
	public Single(String description, int priority) {
		super(description);
	}

	public int getPriority() {
		return this.priority;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(priority);
		return result;
	}

	//	@Override
	//	public boolean equals(Object obj) {
	//		if (this == obj)
	//			return true;
	//		if (!super.equals(obj))
	//			return false;
	//		if (getClass() != obj.getClass())
	//			return false;
	//		Single other = (Single) obj;
	//		return priority == other.priority;
	//	}

	@Override
	public boolean occursOn(LocalDate date) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String toString() {
		return "Single [priority=" + priority + "]";
	}

}
