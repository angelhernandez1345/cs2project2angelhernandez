package edu.westga.cs1302.project2.model;

/**
 * The enum type.
 * 
 * @author Angel Hernandez
 * @version 10/15/2021
 *
 */
public enum AppointmentType {
	SINGLE, DAILY, MONTHLY, YEARLY
}
